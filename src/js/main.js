/**
 * Created by Dims on 20.03.2018.
 */
$(document).ready(function() {
    var owl = $('.owl-carousel');
    owl.owlCarousel({
        items: 1,
        singleItems: true,
        nav: false,
        loop: true,
        startPosition: 7,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1
            },
            1000: {
                items: 1
            }
        }

    })
})

$(document).ready(function () {
    $('.popup_link').magnificPopup();
});